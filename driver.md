---
layout: page
title: Driver
permalink: /driver/
---

Choose your look
----------------

- **Eyes**: laughing, cool, hard, cold, distant, artificial
- **Face**: blank,
covered, attractive,
decorated, rugged,
thin
- **Body**: toned, lithe,
compact, scarred,
augmented, flabby,
unfit
- **Wear**: flashy,
formal, casual,
utility, scrounge,
vintage, leathers,
military, corporate

Cyberwear
---------
You get:

**Neural Interface** with **Remote Control Module**: Allows direct neural control of
an appropriately configured external device such as a vehicle, weapon, recording
device, or hacked electronic system. The RCM gives the interface wireless broadcast
and reception capacity to allow the remote control of vehicles and drones. *Choose
two of following tags: +encrypted, +multi-tasking, +inaccessible partition*.